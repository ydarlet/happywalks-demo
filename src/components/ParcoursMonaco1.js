import React from 'react';

const ParcoursMonaco1 = () => (
    <div className="parcours1">



        <div className="parcours-container parcours-container__title background1">
            <img className="parcours-container__image" src="./images/parcours/all-in-one-day/casino.jpg"/>
            <h1>All in one day</h1>
            <div className="parcours-container__cartouche">
                <div className="cartouche_hour">6h45</div>
                <img className="icon_chronos" src="./images/icons/cartouche/icons8-temps-500.png"/>
                <div className="cartouche_pays">Monaco</div>
                <img className="icon_comment" src="./images/icons/cartouche/icons8-sujet-100.png"/>
                <div className="cartouche_comment">2</div>
                <img className="icon_rank" src="./images/icons/cartouche/icons8-étoile-de-noel-52.png"/>
                <div className="cartouche_rank">4.5</div>
            </div>
            <div className="parcours-container__cartouche">
                <div className="petit_texte">
                    Une journée sur le rocher, découvrir les incontournables de Monaco en un clin d'œil !
                </div>
                <img className="cartouche_plus" src="./images/icons/cartouche/keyboard-right-arrow-button.png"/>
            </div>
        </div>



    </div>
);

export default ParcoursMonaco1;