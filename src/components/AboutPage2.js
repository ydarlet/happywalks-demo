import React from 'react';
import { Link } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import BurgerMenu from './BurgerMenu';
import AboutPage from './AboutPage';

const AboutPage2 = () => (
    <div>
        <Header />
        <BurgerMenu />

        <div className="about2-container">
            <center><h1 className="classic-login__h1 about2-textcolor about2-resize-h1">À propos</h1></center>
            <br/>
            <center><img className="box-layout__image" src="/images/logo3-yellow.png" width="100" /></center>
            <br/>
            <h4 className="classic-login__h4 about2-textcolor2">Happywalks</h4>
            <h4 className="classic-login__h4 about2-textcolor">Application Smart Tourisme qui vous fait voyager selon vos préférences !</h4>
            <br/>
            <center><img className="box-layout__image" src="/images/about/fusee.png" width="100" /></center>
            <br/>
            <h4 className="classic-login__h4 about2-textcolor2">Start-up...</h4>
            <h4 className="classic-login__h4 about2-textcolor">...Early stage qui compte améliorer le monde du tourisme pour tous ses acteurs !</h4>
            <br/>
            <center><img className="box-layout__image" src="/images/about/42.png" width="100" /></center>
            <br/>
            <h4 className="classic-login__h4 about2-textcolor2">Ecole 42</h4>
            <h4 className="classic-login__h4 about2-textcolor">Partenaire du projet avec l'association Matrice ainsi que des étudiants universitaires issus de masters en tourisme.</h4>
            <br/>
            <center><img className="box-layout__image" src="/images/about/matrice-monaco-3.png" width="130" /></center>
            <br/>
            <center><img className="box-layout__image" src="/images/about/iut-mlv-c.png" width="100" /></center>
            <br/>
            <center><img className="box-layout__image" src="/images/about/sorbonne2.png" width="100" /></center>
            <br/>
            <h4 className="classic-login__h4 about2-textcolor2">Monaco</h4>
            <h4 className="classic-login__h4 about2-textcolor">Le gouvernement principer partenaire du projet ainsi que nombre de ses institutions.</h4>
            <br/>
            <center><img className="box-layout__image" src="/images/about/monaco-gov-2.png" width="100" /></center>
            <br/>
            <center><img className="box-layout__image" src="/images/about/tourism-monaco-2.png" width="120" /></center>
            <br/>
            <h4 className="classic-login__h4 about2-textcolor2">Equipe</h4>
            <h4 className="classic-login__h4 about2-textcolor">
                Yann ( CTO - Développeur) <br/>
                Maïlys, Nazanin et Shihui (Expertise Touristique) <br/>
            </h4>
            <br/>
            <br/>
            <h4 className="classic-login__h4 about2-textcolor2">Contact</h4>
            <h4 className="classic-login__h4 about2-textcolor">
                contact@happywalks.fr
            </h4>
            <br/>
        </div>

        <Footer />
    </div>
);

export default AboutPage2;