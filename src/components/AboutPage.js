import React from 'react';
import { connect } from 'react-redux';
import { startLoginGoogle } from '../actions/auth';
import { Link } from 'react-router-dom';

export const AboutPage = ({ startLoginGoogle }) => (
    <div className="classic-login__box-layout gradient--color">
        <h1 className="classic-login__h1">About</h1>
        <br/>
        <h4 className="classic-login__h4">Happywalks</h4>
        <h4 className="classic-login__h4">Application Smart Tourisme qui vous fait voyager selon vos préférences !</h4>
        <br/>
        <center><img className="box-layout__image" src="/images/about/fusee.png" width="100" /></center>
        <br/>
        <h4 className="classic-login__h4">Start-up...</h4>
        <h4 className="classic-login__h4">...Early stage qui compte améliorer le monde du tourisme pour tous ses acteurs !</h4>
        <br/>
        <center><img className="box-layout__image" src="/images/about/42.png" width="100" /></center>
        <br/>
        <h4 className="classic-login__h4">Ecole 42</h4>
        <h4 className="classic-login__h4">Partenaire du projet avec l'association Matrice ainsi que des étudiants universitaires issus de masters en tourisme.</h4>
        <br/>
        <center><img className="box-layout__image" src="/images/about/matrice-monaco-3.png" width="130" /></center>
        <br/>
        <center><img className="box-layout__image" src="/images/about/iut-mlv-c.png" width="100" /></center>
        <br/>
        <center><img className="box-layout__image" src="/images/about/sorbonne2.png" width="100" /></center>
        <br/>
        <h4 className="classic-login__h4">Monaco</h4>
        <h4 className="classic-login__h4">Le gouvernement principer partenaire du projet ainsi que nombre de ses institutions.</h4>
        <br/>
        <center><img className="box-layout__image" src="/images/about/monaco-gov-2.png" width="100" /></center>
        <br/>
        <center><img className="box-layout__image" src="/images/about/tourism-monaco-2.png" width="120" /></center>
        <br/>
        <h4 className="classic-login__h4">Equipe</h4>
        <h4 className="classic-login__h4">
            Yann (CTO - Développeur) <br/>
            Maïlys, Nazanin et Shihui (Expertise Touristique) <br/>
        </h4>
        <br/>
        <br/>
        <h4 className="classic-login__h4">Contact</h4>
        <h4 className="classic-login__h4">
            contact@happywalks.fr
        </h4>
        <br/>
        <button 
            className="classic-login__button2 classic-login__button--facebook classic-login__button--radius"
            >
            <Link className="classic-login__link" to="/classic-login">
                Sign In!
            </Link>
        </button>
        <button 
            className="classic-login__button classic-login__button--facebook classic-login__button--radius"
            >Login with Facebook
        </button>
        <button 
            className="classic-login__button classic-login__button--google classic-login__button--radius"
            onClick={startLoginGoogle}>Login with Google
        </button>
        <br/>
        <div className="classic-login__create--account--text">
            <h4>We don't know each other yet ? &nbsp;
                <Link className="classic-login__link" to="/create-account">
                 Sign Up !
                </Link>
            </h4>
        </div>
    </div>
);


const mapDispatchToProps = (dispatch) => ({
    startLoginGoogle: () => dispatch(startLoginGoogle())
});

export default connect(undefined, mapDispatchToProps)(AboutPage);
// export default AboutPage;