import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { startLogout } from '../actions/auth';


export const Header = ({ startLogout }) => (
    <header className="header gradient--color">
        <div className="content-container">
            <div className="header__content">
                <Link className="header__title" to="/catalogue">
                    <h1>happywalks</h1>
                </Link>
            </div>
        </div>
    </header>
);

// export const Header = ({ startLogout }) => (
//     <header className="header gradient--color">
//         <div className="content-container">
//             <div className="header__content">
//                 <Link className="header__title" to="/catalogue">
//                     <h1>happywalks</h1>
//                 </Link>
//                 <button className="button button--link button--logout" onClick={startLogout}>Logout</button>
//             </div>
//         </div>
//     </header>
// );

const mapDispatchToProps = (dispatch) => ({
    startLogout: () => dispatch(startLogout())
});

export default connect(undefined, mapDispatchToProps)(Header);