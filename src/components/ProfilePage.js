import React from 'react';
import { Link } from 'react-router-dom';
import { color, toggleColor } from '../actions/profil';
import { connect } from 'react-redux';

// <label className="radio__container"> 1ère fois
//             <input type="radio" defaultChecked="checked" name="first-time"/>
//             <span className="checkmark"></span>
//         </label>
//         <label className="radio__container"> Habitué
//             <input type="radio" name="first-time"/>
//             <span className="checkmark"></span>
//         </label>

function buttonFunction(el) {
  el.style.backgroundColor = "red";
}

// function onClickTag(e) {
//     //console.log('Tag Cliqué');
//     //console.log(e.innerHTML);
//     //console.log(e);
//     if (e.className.search('is-primary') != -1) {
//         e.className = 'tag is-medium';
//     } else {
//         e.className = 'tag is-primary is-medium';
//     }
// }

function onClickParam(e) {
    if (e.className.search('isOn') != -1) {
        e.className = 'icon__tourist_2';
    } else {
        e.className = 'icon__tourist_2 isOn';
    }
}

// var icon_slow = document.getElementById('icon_slow');
// icon_slow.onClick = function() {
//     onClickParam(icon_slow);
// }

export const ProfilePage = (toggleColor) => (
    <div className="profile__div">
        <img className="profile__img image-cropper" src="./images/profile/profile-pic/user2.png"/>
        <br/>
        <div className="tourist_name">
            Yann Darlet
        </div>
        <br/><br/>
        <Link to="/modif-profil-infos">
            <button 
                className="profile_btn_modif"
                >Modifier mes<br/>données personnelles
            </button>
        </Link>
        <br/>
        <button 
            className="profile_btn_parcours_perso"
            >Surprends moi ! <br/> (itinéraire personnalisé)
        </button>
        <br/><br/>
        <h1>Visite</h1>
        <img className="icon__tourist" src="./images/icons/profile/icons8-touriste-homme-52.png"/>
        <br/>
        <div className="container__visite">
            <button className="button__visite" onClick={buttonFunction}>1ère fois</button>
            <button className="button__visite">Habitué</button>
        </div>
        <br/><br/>
        <h1>Temps</h1>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img    
                    id="icon_slow"
                    className="icon__tourist_2 "
                    src="./images/icons/profile/icons8-tortue-52.png"
                    onClick={toggleColor}/>
                Décontracté
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-marche-52.png"/>
                Modéré
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-exercice-90.png"/>
                Pressé
            </div>
        </div>
        <br/>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-montre-96.png"/>
                +Heures
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-horloge2-96.png"/>
                1 Jour
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-horloge-96.png"/>
                +Jours
            </div>
        </div>
        <br/><br/>
        <h1>Personnalisation</h1>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-romance-filled-100.png"/>
                Duo
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-contacts-52.png"/>
                Solo
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-user-groups-filled-500.png"/>
                Famille
            </div>
        </div>
        <br/>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-children-52.png"/>
                Enfants
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-lol-52.png"/>
                Amis
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-manager-52.png"/>
                Business
            </div>
        </div>
        <br/>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-pregnant-52.png"/>
                Enceinte
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-stroller-filled-500.png"/>
                Poussette
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-babys-room-90.png"/>
                Bébé
            </div>
        </div>
        <br/>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-wheelchair-52.png"/>
                Handicapé
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-blind-52.png"/>
                Malvoyant
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-deaf-52.png"/>
                Malentendant
            </div>
        </div>
        <br/>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-camera-90.png"/>
                Photographe
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-trekking-filled-500.png"/>
                Randonnée
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-romance-52.png"/>
                Romantique
            </div>
        </div>
        <br/>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-soleil-500.png"/>
                Day
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-dog-90.png"/>
                Animaux
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-walking-stick-96.png"/>
                Agé
            </div>
        </div>
        <br/>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-symbole-de-la-lune-480.png"/>
                Night
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-un-doigt-500.png"/>
                Interactif
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-réalité-augmentée-filled-500.png"/>
                Augmenté
            </div>
        </div>
        <br/>
        <div className="profile_div_1">
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-arbre-à-feuilles-caduques-96.png"/>
                Nature
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-panier-filled-500.png"/>
                Local
            </div>
            <div className="profile_div_2">
                <img className="icon__tourist_2" src="./images/icons/profile/icons8-sac-de-courses-filled-50.png"/>
                Shopping
            </div>
        </div>
        <br/><br/><br/><br/>
    </div>
);

function mapStateToProps(state) {
    return {
        profil: state.profil
    };
};

const mapDispatchToProps = (dispatch) => ({
    toggleColor: () => dispatch(toggleColor())
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);