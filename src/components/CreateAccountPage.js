import React from 'react';
import { connect } from 'react-redux';
import { startLoginGoogle } from '../actions/auth';
import { Link } from 'react-router-dom';

export const LoginPage = ({ startLoginGoogle }) => (
    <div className="classic--create__box-layout gradient--color">
        <h1 className="classic--create__h1">Let's get you a profile</h1>
        <br/>
        <div className="classic-div-profile-icns">
            <img className="classic--create__image" src="/images/profile/kids_avatar/icons8-plus-math-2-52.png"/>
            <img className="classic--create__image classic--create__image_3" src="/images/profile/kids_avatar/boy-22.png"/>
            <img className="classic--create__image classic--create__image_2" src="/images/profile/kids_avatar/girl-16.png"/>
            <img className="classic--create__image classic--create__image_2" src="/images/profile/kids_avatar/boy-18.png"/>
            <img className="classic--create__image classic--create__image_2" src="/images/profile/kids_avatar/girl-22.png"/>
        </div>
        <br/>
        <form className="classic--create__form">
            <h4 className="classic--create__h4">Prénom *</h4>
            <input
                type="text"
                placeholder="Prénom..."
                className="classic--create__text-input classic--create__extra-margin"
            />
            <h4 className="classic--create__h4">Nom *</h4>
            <input
                type="text"
                placeholder="Nom..."
                className="classic--create__text-input classic--create__extra-margin"
            />
            <h4 className="classic--create__h4">Email *</h4>
            <input
                type="text"
                placeholder="Email..."
                className="classic--create__text-input classic--create__extra-margin"
            />
            <h4 className="classic--create__h4">Password *</h4>
            <input
                type="password"
                placeholder="Password..."
                className="classic--create__text-input classic--create__extra-margin"
            />
        </form>
        <div className="classic--create__terms">
            <h5 className="classic--create__h5">
                By signing up you accept our <u>Terms</u> and <u>Privacy Policy</u>
            </h5>
        </div>
        <button 
            className="classic--create__button2 classic--create__button--facebook classic--create__button--radius"
            >Sign Up!
        </button>
        <br/>
        <button 
            className="classic--create__button classic--create__button--facebook classic--create__button--radius"
            >or Contninue with Facebook
        </button>
        <button 
            className="classic--create__button classic--create__button--google classic--create__button--radius"
            onClick={startLoginGoogle}>or Continue with Google
        </button>
        <br/>
        <div className="classic--create__create--account--text">
            <h4>We already know each other ? &nbsp;
                <Link className="classic-login__link" to="/classic-login">
                 Sign In !
                </Link>
            </h4>
        </div>
        <br/>
    </div>
);

const mapDispatchToProps = (dispatch) => ({
    startLoginGoogle: () => dispatch(startLoginGoogle())
});

export default connect(undefined, mapDispatchToProps)(LoginPage);