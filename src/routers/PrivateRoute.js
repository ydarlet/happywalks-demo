import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import Header from '../components/Header';
import Footer from '../components/Footer';
import BurgerMenu from '../components/BurgerMenu';
// import { parcours } from '../actions/parcours';

export const PrivateRoute = ({
    isAuthenticated,
    component: Component,
    ...rest
}) => (
    <Route {...rest} component={(props) => (
        isAuthenticated ? (
            <div>
                <Header />
                <BurgerMenu right width={ 200 } />
                <Component {...props} />
                <Footer />
            </div>
        ) : (
            <Redirect to="/" />
        )
    )} />
);

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.auth.uid
});

// const mapDispatchToProps = (dispatch) => ({
//     parcours: () => dispatch(parcours())
// });

// export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
export default connect(mapStateToProps)(PrivateRoute);