import React from 'react';
import { connect } from 'react-redux';
import { startLoginGoogle } from '../actions/auth';
import { Link } from 'react-router-dom';

export const LoginPage = ({ startLoginGoogle }) => (
    <div className="classic-login__box-layout gradient--color">
        <h1 className="classic-login__h1">Log in</h1>
        <br/>
        <form className="classic-login__form">
            <h4 className="classic-login__h4">Email</h4>
            <input
                type="text"
                placeholder="please type your email..."
                className="classic-login__text-input classic-login__extra-margin"
            />
            <h4 className="classic-login__h4">Password</h4>
            <input
                type="password"
                placeholder="please type your password..."
                className="classic-login__text-input classic-login__extra-margin"
            />
        </form>
        <br/>
        <button 
            className="classic-login__button2 classic-login__button--facebook classic-login__button--radius"
            >Sign In!
        </button>
        <br/>
        <button 
            className="classic-login__button classic-login__button--facebook classic-login__button--radius"
            >or Contninue with Facebook
        </button>
        <button 
            className="classic-login__button classic-login__button--google classic-login__button--radius"
            onClick={startLoginGoogle}>or Continue with Google
        </button>
        <br/>
        <div className="classic-login__create--account--text">
            <h4>We don't know each other yet ? &nbsp;
                <Link className="classic-login__link" to="/create-account">
                 Sign Up !
                </Link>
            </h4>
        </div>
    </div>
);

const mapDispatchToProps = (dispatch) => ({
    startLoginGoogle: () => dispatch(startLoginGoogle())
});

export default connect(undefined, mapDispatchToProps)(LoginPage);