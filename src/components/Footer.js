import React from 'react';
import { Link } from 'react-router-dom';

export const Footer = () => (
    <header className="footer gradient--color">
        <div className="content-container">
            <div className="footer__content">
                <Link className="footer__title footer__title__margin" to="/catalogue">
                    <h1>parcours</h1>
                </Link>
                <Link className="footer__title" to="/profile">
                    <h1>profil</h1>
                </Link>
            </div>
        </div>
    </header>
);


export default Footer;