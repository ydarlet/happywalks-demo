import React from 'react';
import { connect } from 'react-redux';
import { startLoginGoogle } from '../actions/auth';
import { Link } from 'react-router-dom';

export const LoginPage = ({ startLoginGoogle }) => (
    <div className="box-layout gradient--color">
        <div className="box-layout__box">
            <img className="box-layout__image" src="/images/logo3-yellow.png" width="100" />
            <h1 className="box-layout__title bold">happywalks</h1>
            <p className="bold">Enjoy visit any place!</p>
            <button 
                className="button button--facebook button--radius"
                >Log in with Facebook
            </button>
            <button 
                className="button button--google button--radius"
                onClick={startLoginGoogle}>Log in with Google
            </button>
            <Link to="/classic-login">
                <button 
                    className="button button--classic-login button--radius"
                    >Log in
                </button>
            </Link>
            <Link to="/create-account">
                <button 
                    className="button button--create-account button--radius"
                    >Create an account
                </button>
            </Link>
            <Link to="/about">
                <button 
                    className="button button--about button--radius"
                    >About
                </button>
            </Link>
        </div>
    </div>
);

const mapDispatchToProps = (dispatch) => ({
    startLoginGoogle: () => dispatch(startLoginGoogle())
});

export default connect(undefined, mapDispatchToProps)(LoginPage);