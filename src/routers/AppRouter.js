import React from 'react';
import createHistory from 'history/createBrowserHistory';
import { Router, Route, Switch } from 'react-router-dom';
import CataloguePage from '../components/CataloguePage';
import ProfilePage from '../components/ProfilePage';
import NotFoundPage from '../components/NotFoundPage';
import LoginPage from '../components/LoginPage';
import ClassicLoginPage from '../components/ClassicLoginPage';
import CreateAccountPage from '../components/CreateAccountPage';
import ModifProfilPage from '../components/ModifProfilPage';
import AboutPage from '../components/AboutPage';
import AboutPage2 from '../components/AboutPage2';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import ParcoursMonaco1 from '../components/ParcoursMonaco1';

export const history = createHistory();

const AppRouter = () => (
        <Router history={history}>
        <div>
            <Switch>
                <PublicRoute path="/" component={LoginPage} exact={true}/>
                <PublicRoute path="/classic-login" component={ClassicLoginPage} exact={true}/>
                <PublicRoute path="/create-account" component={CreateAccountPage} exact={true}/>
                <PublicRoute path="/about" component={AboutPage} exact={true}/>
                <PrivateRoute path="/catalogue" component={CataloguePage}/>
                <PrivateRoute path="/profile" component={ProfilePage}/>
                <PrivateRoute path="/modif-profil-infos" component={ModifProfilPage}/>
                <PrivateRoute path="/monaco-all-in-one-day" component={ParcoursMonaco1}/>
                <PrivateRoute path="/about-2" component={AboutPage2}/>
                <Route component={NotFoundPage}/>
            </Switch>
        </div>
        </Router>
);

export default AppRouter;