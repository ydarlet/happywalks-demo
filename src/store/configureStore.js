import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import authReducer from '../reducers/auth';
import profilReducer from '../reducers/profil';
import parcoursReducer from '../reducers/parcours';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        combineReducers({
            auth: authReducer,
            profil: profilReducer,
            parcours: parcoursReducer
        }),
        composeEnhancers(applyMiddleware(thunk))
    );
    
    return store;
};

