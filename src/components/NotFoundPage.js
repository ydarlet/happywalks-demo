import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/Header';
import Footer from '../components/Footer';
import BurgerMenu from '../components/BurgerMenu';

const NotFoundPage = () => (
    <div>
        <Header />
        <BurgerMenu right width={ 200 } />
        <div className="erreur_404">
            <br/><br/>
            <div className="erreur_404_container1">
                <div className="title_woops">Woops !</div>
                <div className="title_404">Erreur 404</div>
                La page demandée n'existe pas.
                <br/><br/>
                Visitez plutôt le catalogue des <Link to="/catalogue">Parcours</Link>
                <br/>
                Ou bien la page de votre <Link to="/profile">Profil</Link>
            </div>
        </div>
        <Footer />
    </div>
);

export default NotFoundPage;

// import React from 'react';
// import { Link } from 'react-router-dom';
// import { connect } from 'react-redux';
// import { Route, Redirect } from 'react-router-dom';

// export const NotFoundPage = ({
//     isAuthenticated,
//     component: Component,
//     ...rest
// }) => (
//     <Route {...rest} component={(props) => (
//         isAuthenticated ? (
//             <div>
//                 404 - <Link to="/">Go home Private</Link>
//             </div>
//         ) : (
//             <div>
//                 404 - <Link to="/">Go home Public</Link>
//             </div>
//         )
//     )} />
// );

// const mapStateToProps = (state) => ({
//     isAuthenticated: !!state.auth.uid
// });

// export default connect(mapStateToProps)(NotFoundPage);