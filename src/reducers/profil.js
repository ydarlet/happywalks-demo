const initialState = {
    slow: false,
    moderate: false,
    fast: false,
    hours: false,
    day: false,
    days: false,
    duo: false,
    solo: false,
    family: false,
    children: false,
    friends: false,
    business: false,
    pregnant: false,
    stroller: false,
    baby: false,
    handicaped: false,
    blind: false,
    deaf: false,
    photograph: false,
    hiker: false,
    romantic: false,
    sunlight: false,
    moonlight: false,
    pets: false,
    old: false,
    interactive: false,
    augmented: false,
    nature: false,
    local: false,
    shopping: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'TOGGLE_COLOR':
            return {
                // slow: !state.slow
                slow: true
            };
        default:
            return state;
    }
}