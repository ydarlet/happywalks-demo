export default function() {
    return [
        {
            name: 'All in One Day',
            description: 'Une journée sur le rocher, découvrir les incontournables de Monaco en un clin d\'œil !',
            duration: 413,
            distance: 7353,
            country: 'Monaco',
            comments: 2,
            rank: 4.5,
            img: 'all-in-one-day/casino.jpg'
        },
        {
            name: 'Beausoleil',
            description: 'Rencontrez la population et découvrez les produits locaux !',
            duration: 294,
            distance: 2870,
            country: 'Monaco',
            comments: 10,
            rank: 5,
            img: 'beausoleil/beausoleil1.jpg'
        },
        {
            name: 'Détente Azurée',
            description: 'Avec ce parcours, une détente azurée vous est assurée !',
            duration: 385,
            distance: 4960,
            country: 'Monaco',
            comments: 3,
            rank: 3.5,
            img: 'detente-azuree/larvotto3.jpg'
        },
        {
            name: 'Éclat de Vert',
            description: 'Grande promenade dans Monaco, une ville verte et ravissante.',
            duration: 629,
            distance: 9682,
            country: 'Monaco',
            comments: 7,
            rank: 4.5,
            img: 'eclat-vert/japonais2.jpg'
        },
        {
            name: 'Pépites d\'Or',
            description: 'Découvrez la beauté de Monaco le soir, pétillante comme de l’or !',
            duration: 215,
            distance: 3079,
            country: 'Monaco',
            comments: 12,
            rank: 5,
            img: 'pepites/pepite2.jpeg'
        },
        {
            name: 'Grand Prix',
            description: 'Revivez la course mythique en parcourant à pied chacune de ses étapes clefs.',
            duration: 267,
            distance: 3185,
            country: 'Monaco',
            comments: 25,
            rank: 4,
            img: 'grand-prix/grand-prix.jpg'
        }
    ];
}


// export default (state = {}, action) => {
//     switch (action.type) {
//         case 'PARCOURS':
//             return {
//                 liste: {
//                     p1:{
//                         name: 'All in One Day',
//                         description: 'Découvrez les incontournables de Monaco en une journée !',
//                         duration: 413,
//                         distance: 7353
//                     },
//                     p2:{
//                         name: 'Beausoleil',
//                         description: 'Rencontrez la population et découvrez les produits locaux !',
//                         duration: 294,
//                         distance: 2870
//                     },
//                     p3:{
//                         name: 'Détente Azurée',
//                         description: 'Avec ce parcours, une détente azurée vous est assurée !',
//                         duration: 385,
//                         distance: 4960
//                     },
//                     p4:{
//                         name: 'Éclat de Vert',
//                         description: 'Grande promenade dans Monaco, une ville verte et ravissante.',
//                         duration: 629,
//                         distance: 9682
//                     },
//                     p5:{
//                         name: 'Pépites d\'Or',
//                         description: 'Découvrez la beauté de Monaco le soir, pétillante comme de l’or !',
//                         duration: 215,
//                         distance: 3079
//                     },
//                     p6:{
//                         name: 'Grand Prix',
//                         description: 'Revivez la course mythique en parcourant à pied chacune de ses étapes clefs.',
//                         duration: 267,
//                         distance: 3185
//                     }
//                 }
//             };
//         default:
//             return state;
//     }
// }