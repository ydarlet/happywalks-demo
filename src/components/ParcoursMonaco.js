import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';






class ParcoursMonaco extends Component {

    renderParcoursList() {
        return this.props.parcours.map((walk) => {
            return (
                <div key={walk.name} className="parcours-container parcours-container__title background1">
                    <img className="parcours-container__image" src={`./images/parcours/${walk.img}`}/>
                    <h1>{walk.name}</h1>
                    <div className="parcours-container__cartouche">
                        <div className="cartouche_hour">{walk.duration}</div>
                        <img className="icon_chronos" src="./images/icons/cartouche/icons8-temps-500.png"/>
                        <div className="cartouche_pays">{walk.country}</div>
                        <img className="icon_comment" src="./images/icons/cartouche/icons8-sujet-100.png"/>
                        <div className="cartouche_comment">{walk.comments}</div>
                        <img className="icon_rank" src="./images/icons/cartouche/icons8-étoile-de-noel-52.png"/>
                        <div className="cartouche_rank">{walk.rank}</div>
                    </div>
                    <div className="parcours-container__cartouche">
                        <div className="petit_texte">
                            {walk.description}
                        </div>
                        <Link to="/monaco-all-in-one-day">
                            <img className="cartouche_plus" src="./images/icons/cartouche/keyboard-right-arrow-button.png"/>
                        </Link>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
            <div className="parcours1">
                {this.renderParcoursList()}
            </div>
        )
    }

}









// const ParcoursMonaco = () => (
//     <div className="parcours1">



//         <div className="parcours-container parcours-container__title background1">
//             <img className="parcours-container__image" src="./images/parcours/all-in-one-day/casino.jpg"/>
//             <h1>All in one day</h1>
//             <div className="parcours-container__cartouche">
//                 <div className="cartouche_hour">6h45</div>
//                 <img className="icon_chronos" src="./images/icons/cartouche/icons8-temps-500.png"/>
//                 <div className="cartouche_pays">Monaco</div>
//                 <img className="icon_comment" src="./images/icons/cartouche/icons8-sujet-100.png"/>
//                 <div className="cartouche_comment">2</div>
//                 <img className="icon_rank" src="./images/icons/cartouche/icons8-étoile-de-noel-52.png"/>
//                 <div className="cartouche_rank">4.5</div>
//             </div>
//             <div className="parcours-container__cartouche">
//                 <div className="petit_texte">
//                     Une journée sur le rocher, découvrir les incontournables de Monaco en un clin d'œil !
//                 </div>
//                 <Link to="/monaco-all-in-one-day">
//                     <img className="cartouche_plus" src="./images/icons/cartouche/keyboard-right-arrow-button.png"/>
//                 </Link>
//             </div>
//         </div>

//         <div className="parcours-container parcours-container__title background1">
//             <img className="parcours-container__image" src="./images/parcours/beausoleil/beausoleil1.jpg"/>
//             <h1>Beausoleil</h1>
//             <div className="parcours-container__cartouche">
//                 <div className="cartouche_hour">4h30</div>
//                 <img className="icon_chronos" src="./images/icons/cartouche/icons8-temps-500.png"/>
//                 <div className="cartouche_pays">Monaco</div>
//                 <img className="icon_comment" src="./images/icons/cartouche/icons8-sujet-100.png"/>
//                 <div className="cartouche_comment">10</div>
//                 <img className="icon_rank" src="./images/icons/cartouche/icons8-étoile-de-noel-52.png"/>
//                 <div className="cartouche_rank">5</div>
//             </div>
//             <div className="parcours-container__cartouche">
//                 <div className="petit_texte">
//                     Rencontrez la population et découvrez les produits locaux !
//                 </div>
//                 <img className="cartouche_plus" src="./images/icons/cartouche/keyboard-right-arrow-button.png"/>
//             </div>
//         </div>

//         <div className="parcours-container parcours-container__title background1">
//             <img className="parcours-container__image" src="./images/parcours/detente-azuree/larvotto3.jpg"/>
//             <h1>Détente Azurée</h1>
//             <div className="parcours-container__cartouche">
//                 <div className="cartouche_hour">6h35</div>
//                 <img className="icon_chronos" src="./images/icons/cartouche/icons8-temps-500.png"/>
//                 <div className="cartouche_pays">Monaco</div>
//                 <img className="icon_comment" src="./images/icons/cartouche/icons8-sujet-100.png"/>
//                 <div className="cartouche_comment">3</div>
//                 <img className="icon_rank" src="./images/icons/cartouche/icons8-étoile-de-noel-52.png"/>
//                 <div className="cartouche_rank">3.5</div>
//             </div>
//             <div className="parcours-container__cartouche">
//                 <div className="petit_texte">
//                     Avec ce parcours, une détente azurée vous est assurée !
//                 </div>
//                 <img className="cartouche_plus" src="./images/icons/cartouche/keyboard-right-arrow-button.png"/>
//             </div>
//         </div>



//         <div className="parcours-container parcours-container__title background1">
//             <img className="parcours-container__image" src="./images/parcours/eclat-vert/japonais2.jpg"/>
//             <h1>Éclat de Vert</h1>
//             <div className="parcours-container__cartouche">
//                 <div className="cartouche_hour">10h25</div>
//                 <img className="icon_chronos" src="./images/icons/cartouche/icons8-temps-500.png"/>
//                 <div className="cartouche_pays">Monaco</div>
//                 <img className="icon_comment" src="./images/icons/cartouche/icons8-sujet-100.png"/>
//                 <div className="cartouche_comment">7</div>
//                 <img className="icon_rank" src="./images/icons/cartouche/icons8-étoile-de-noel-52.png"/>
//                 <div className="cartouche_rank">4.5</div>
//             </div>
//             <div className="parcours-container__cartouche">
//                 <div className="petit_texte">
//                     Grande promenade dans Monaco, une ville verte et ravissante.
//                 </div>
//                 <img className="cartouche_plus" src="./images/icons/cartouche/keyboard-right-arrow-button.png"/>
//             </div>
//         </div>



//         <div className="parcours-container parcours-container__title background1">
//             <img className="parcours-container__image" src="./images/parcours/pepites/pepite2.jpeg"/>
//             <h1>Pépites d'Or</h1>
//             <div className="parcours-container__cartouche">
//                 <div className="cartouche_hour">3h30</div>
//                 <img className="icon_chronos" src="./images/icons/cartouche/icons8-temps-500.png"/>
//                 <div className="cartouche_pays">Monaco</div>
//                 <img className="icon_comment" src="./images/icons/cartouche/icons8-sujet-100.png"/>
//                 <div className="cartouche_comment">12</div>
//                 <img className="icon_rank" src="./images/icons/cartouche/icons8-étoile-de-noel-52.png"/>
//                 <div className="cartouche_rank">5</div>
//             </div>
//             <div className="parcours-container__cartouche">
//                 <div className="petit_texte">
//                     Découvrez la beauté de Monaco le soir, pétillante comme de l’or !
//                 </div>
//                 <img className="cartouche_plus" src="./images/icons/cartouche/keyboard-right-arrow-button.png"/>
//             </div>
//         </div>



//         <div className="parcours-container parcours-container__title background1">
//             <img className="parcours-container__image" src="./images/parcours/grand-prix/grand-prix.jpg"/>
//             <h1>Grand Prix de Monaco</h1>
//             <div className="parcours-container__cartouche">
//                 <div className="cartouche_hour">4h20</div>
//                 <img className="icon_chronos" src="./images/icons/cartouche/icons8-temps-500.png"/>
//                 <div className="cartouche_pays">Monaco</div>
//                 <img className="icon_comment" src="./images/icons/cartouche/icons8-sujet-100.png"/>
//                 <div className="cartouche_comment">25</div>
//                 <img className="icon_rank" src="./images/icons/cartouche/icons8-étoile-de-noel-52.png"/>
//                 <div className="cartouche_rank">4</div>
//             </div>
//             <div className="parcours-container__cartouche">
//                 <div className="petit_texte">
//                     Revivez la course mythique en parcourant à pied chacune de ses étapes clefs.
//                 </div>
//                 <img className="cartouche_plus" src="./images/icons/cartouche/keyboard-right-arrow-button.png"/>
//             </div>
//         </div>



//     </div>
// );

function mapStateToProps(state) {
    return {
        parcours: state.parcours
    };
};

export default connect(mapStateToProps)(ParcoursMonaco);