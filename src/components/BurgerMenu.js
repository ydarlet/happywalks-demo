import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { startLogout } from '../actions/auth';
import { slide as Menu } from 'react-burger-menu'


export const BurgerMenu = ({ startLogout }) => (
    <Menu right width={ 200 }>
      <Link to="/catalogue">
        <span className="menu-item">Parcours</span>
      </Link>
      <Link to="/profile">
        <span className="menu-item">Profil</span>
      </Link>
      <br/>
      <Link to="/help">
        <span className="menu-item">Aide</span>
      </Link>
      <Link to="/about-2">
        <span className="menu-item">À propos</span>
      </Link>
      <Link to="/contact">
        <span className="menu-item">Contact</span>
      </Link>
      <br/>
      <Link to="/">
        <span onClick={startLogout} className="menu-item">Déconnexion</span>
      </Link>
    </Menu>
);

// // Good One
// export default props =>  {
//     return (
//       <Menu {...props}>
//         <a id="contact" className="menu-item" href="/contact">Parcours</a>
//         <a id="contact" className="menu-item" href="/contact">Profil</a>
        
//         <br/>
//         <a id="home" className="menu-item" href="/">Aide</a>
//         <a id="about" className="menu-item" href="/about">À propos</a>
//         <a id="contact" className="menu-item" href="/contact">Contact</a>

//         <br/>
//         <a id="contact" className="menu-item" href="/contact">Déconnexion</a>
//       </Menu>
//     );
// };

const mapDispatchToProps = (dispatch) => ({
    startLogout: () => dispatch(startLogout())
});

export default connect(undefined, mapDispatchToProps)(BurgerMenu);

// export default props =>  {
//     return (
//       <Menu {...props}>
//         <a id="home" className="menu-item" href="/">Home</a>
//         <a id="about" className="menu-item" href="/about">About</a>
//         <a id="contact" className="menu-item" href="/contact">Contact</a>
//         <a onClick={ this.showSettings } className="menu-item--small" href="">Settings</a>
//       </Menu>
//     );
// };

// class Menu1 extends React.Component {
//   showSettings (event) {
//     event.preventDefault();
//     .
//     .
//     .
//   }
 
//   render () {
//     return (
//       <Menu>
//         <a id="home" className="menu-item" href="/">Home</a>
//         <a id="about" className="menu-item" href="/about">About</a>
//         <a id="contact" className="menu-item" href="/contact">Contact</a>
//         <a onClick={ this.showSettings } className="menu-item--small" href="">Settings</a>
//       </Menu>
//     );
//   }
// }