import React from 'react';
import SearchBar from './SearchBar';
import Parcours from './Parcours';


const CataloguePage = () => (
    <div>
        <div className="dashboard-margin-bottom">
            <SearchBar />
            <Parcours />
        </div>
    </div>
);

export default CataloguePage;