import React from 'react';

const SearchBar = () => (
    <div>
        <input className="search-bar" type="text" name="search" placeholder="Search..."/>
    </div>
);

export default SearchBar;